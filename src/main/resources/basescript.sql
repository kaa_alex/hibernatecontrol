create database hibernate;

use hibenate;

CREATE TABLE Person (
person_id int(5) PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(50) NOT NULL,
last_name VARCHAR(50) NOT NULL) DEFAULT CHARSET=utf8;

CREATE TABLE Driver_License (
person_id int(5) PRIMARY KEY,
license_number int(15) NOT NULL,
issue_date VARCHAR(10) NOT NULL,
expery_date VARCHAR(10) NOT NULL,
KEY PERSON_TO_LICENSE (person_id),
CONSTRAINT PERSON_TO_USERINFORMATION FOREIGN KEY (person_id)
REFERENCES Person (person_id) ON DELETE CASCADE ON UPDATE CASCADE
) DEFAULT CHARSET=UTF8;

CREATE TABLE Car(
car_id int(5) primary key auto_increment,
person_id int(5) not null,
car_name VARCHAR(255) NOT NULL,
car_Number int(5) NOT NULL,
KEY PERSON_TO_CAR (person_id),
CONSTRAINT PERSON_TO_CAR FOREIGN KEY (person_id)
REFERENCES Person (person_id) ON DELETE CASCADE ON UPDATE CASCADE
) DEFAULT CHARSET=UTF8;


CREATE TABLE Work_Parking (
parking_id int(5) PRIMARY KEY auto_increment,
parking_name VARCHAR(255) NOT NULL,
distance_to_place int(10) NOT NULL) 
DEFAULT CHARSET=UTF8;

CREATE TABLE Person_Parking (
person_id int(5) NOT NULL,
parking_id int(5) NOT NULL,
PRIMARY KEY (person_id,parking_id),
CONSTRAINT PERSON FOREIGN KEY (person_id) REFERENCES Person (person_id),
CONSTRAINT PARKING FOREIGN KEY (parking_id) REFERENCES Work_parking (parking_id)
) DEFAULT CHARSET=UTF8;
package ru.mail.cops2002;

import java.io.Serializable;

public class Car implements Serializable {

	private static final long serialVersionUID = -4333135505627959930L;
	private Person person;
	private Integer id;
	private String carName;
	private Integer carNumber;

	public Car() {
	}

	public Car(Person person, String carName, Integer carNumber) {
		super();
		this.setPerson(person);
		this.setCarName(carName);
		this.setCarNumber(carNumber);
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public Integer getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(Integer carNumber) {
		this.carNumber = carNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}

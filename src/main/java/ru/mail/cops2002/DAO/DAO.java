package ru.mail.cops2002.DAO;


public interface DAO <O,ID> {
	
	public void delete(ID id);
	
	public void getById(ID id);
	
	public void select(ID id);
	
	public void getAll();
	
	public void create(O entity);
	
	public void update(O entity);

}
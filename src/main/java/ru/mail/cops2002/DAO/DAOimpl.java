package ru.mail.cops2002.DAO;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import ru.mail.cops2002.persistence.*;

public abstract class DAOimpl<O,ID> implements DAO<O,ID> {
	
	 private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	
	@Override
	public void delete(ID id) {
		// TODO Auto-generated method stub
		Session session = null;
		try {
		session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(id);
        session.getTransaction().commit();
		} catch (HibernateException e){
			 session.getTransaction().rollback();
		}
	}

	@Override
	public void getById(ID id) {
		// TODO Auto-generated method stub
	}

	@Override
	public void select(ID id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getAll(){
	// TODO Auto-generated method stub
	}

	@Override
	// TODO Auto-generated method stub
	public void create(O entity) {
		Session session = null;
		try {
		session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
		} catch (HibernateException e){
			 session.getTransaction().rollback();
		}
	}
	
	@Override
	// TODO Auto-generated method stub
		public void update(O entity) {
		Session session = null;
		try {
		session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(entity);
        session.getTransaction().commit();
		} catch (HibernateException e){
			 session.getTransaction().rollback();
		}
	}

}

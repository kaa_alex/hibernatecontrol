package ru.mail.cops2002.DAO;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.mail.cops2002.License;
import ru.mail.cops2002.persistence.HibernateUtil;

public class DriverLicenseDAO<O, ID> extends DAOimpl<O, ID> {
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@Override
	public void delete(ID id) {
		// TODO Auto-generated method stub
		super.delete(id);
	}

	@Override
	public void create(O entity) {
		// TODO Auto-generated method stub
		super.create(entity);
	}

	@Override
	public void update(O entity) {
		// TODO Auto-generated method stub
		super.update(entity);
	}

	public License getByNumber(Integer number) {
		License license = new License();
		Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Query query = session.createQuery("from License where licenseNumber = :number ");
			query.setParameter("number", number);
			license = (License) query.list().get(0);
			session.getTransaction().commit();
		return license;
	}

}

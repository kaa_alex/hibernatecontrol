package ru.mail.cops2002;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Parking implements Serializable {

	private static final long serialVersionUID = -4945523523247524627L;
	private Set<Person> persons = new HashSet<Person>(0);
	private Integer id;
	private String name;
	private Integer distanceToPlace;
	
	public Parking() {
	}

	public Parking(Set<Person> persons, String name, Integer distanceToPlace) {
		super();
		this.persons = persons;
		this.name = name;
		this.distanceToPlace = distanceToPlace;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDistanceToPlace() {
		return distanceToPlace;
	}

	public void setDistanceToPlace(Integer distanceToPlace) {
		this.distanceToPlace = distanceToPlace;
	}

	public Set<Person> getPersons() {
		return persons;
	}

	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}

}

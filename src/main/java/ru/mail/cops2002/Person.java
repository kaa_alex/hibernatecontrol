package ru.mail.cops2002;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Person implements Serializable {

	private static final long serialVersionUID = 8298299690954385874L;
	private Integer id;
	private String firstName;
	private String lastName;
	private License license;
	private Set<Car> cars = new HashSet<Car>(0);
	private Set<Parking> parkings = new HashSet<Parking>(0);

	public Person() {
	}

	public Person(String firstName, String lastName, License license, Set<Car> cars,
			Set<Parking> parkings) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.license = license;
		this.cars = cars;
		this.parkings = parkings;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	public Set<Parking> getParkings() {
		return parkings;
	}

	public void setParkings(Set<Parking> parkings) {
		this.parkings = parkings;
	}

}

package ru.mail.cops2002;

import java.io.Serializable;

public class License implements Serializable {

	private static final long serialVersionUID = -8543972015660542220L;
	private Integer personId;
	private Person person;
	private Integer licenseNumber;
	private String issueDate;
	private String experyDate;
	
	public License() {
	}

	public License(Person person, Integer licenseNumber, String issueDate, String experyDate) {
		super();
		this.setPerson(person);
		this.setLicenseNumber(licenseNumber);
		this.setIssueDate(issueDate);
		this.setExperyDate(experyDate);
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Integer getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(Integer licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getExperyDate() {
		return experyDate;
	}

	public void setExperyDate(String experyDate) {
		this.experyDate = experyDate;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

}

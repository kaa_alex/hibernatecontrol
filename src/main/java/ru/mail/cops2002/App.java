package ru.mail.cops2002;

import org.hibernate.Session;

import ru.mail.cops2002.DAO.DriverLicenseDAO;
import ru.mail.cops2002.persistence.HibernateUtil;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		Person person = new Person();
		person.setFirstName("Alexander");
		person.setLastName("Karpov");
		Car car = new Car();
		car.setCarName("Hyundai Sonata");
		car.setCarNumber(9304);
		Car car1 = new Car();
		car1.setCarName("Mercedes 190");
		car1.setCarNumber(1234);
		car.setPerson(person);
		car1.setPerson(person);
		person.getCars().add(car);
		person.getCars().add(car1);
		License license = new License();
		license.setLicenseNumber(123456);
		license.setIssueDate("01.01.2009");
		license.setExperyDate("01.01.2019");
		person.setLicense(license);
		license.setPerson(person);
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(person);
		session.getTransaction().commit();
		DriverLicenseDAO licenseDAO = new DriverLicenseDAO();
		License license1 = new License();
		license1 = licenseDAO.getByNumber(123456);
		System.out.println(license1);
	}
}
